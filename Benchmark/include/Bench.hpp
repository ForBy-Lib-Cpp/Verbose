// ForBy LTD 2017
// Benchmark Tools v0.2

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FORBY_BENCHMARK_INCLUDED
#define FORBY_BENCHMARK_INCLUDED

#define _forby_benchmark_edition 0x0023

#include <ForBy/Common/ConsoleCodes.h>
#include <ForBy/Logger/Logger.hpp>
#include <ForBy/Utils/StringTools.hpp>

#include <array>
#include <map>
#include <utility>
#include <vector>
#include <cstring>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <functional>
#include <algorithm>
#include <unordered_map>

#define ELAPSED( START , END )  \
    ( double( END - START ) / CLOCKS_PER_SEC )


namespace ForBy{
namespace Verbose{

namespace Benchmark
{

    class Watcher
    {
      public:

            typedef std::array<clock_t, 2> clocks;

            Watcher()
            {
                put("START");
            }

            ~Watcher()
            {

                #ifdef FORBY_DEBUG
                    close_stamp("START");
                    show_timestaps();
                #endif
            }

/*
            template
            < typename F
            , typename ... Args
            >
            inline void Benchmark
            ( const std::string _mark
            , F&& _f
            , Args&& ...args)
            {
                this->new_stamp(_mark);
                _f(std::forward<Args>(args)...);
                this->close_stamp(_mark);
            }
*/

            inline void
            new_stamp( const std::string _mark )
            {
                LOG( LOG_INFO, TEXT_LOW_ON, "### Benchmark label: ",  _mark, LOG_END);
                put(_mark);
            }

            inline double
            close_stamp( const std::string& _mark )
            {
                static clock_t _before;
                double elapsed;


                for( auto& i : labels )
                  if( _mark.compare( i.first ) == 0 )
                  {
                      _before = Stamps.at( i.second );
                      elapsed = ELAPSED( _before, clock() );

                      DEBUG_LOG( LOG_INFO, "Values ", i.first , " || ", i.second, "found when closing", LOG_END );

                      Records.emplace_back
                      (
                          _mark.c_str()
                      ,   elapsed
                      );

                      return elapsed;
                  }

                LOG( LOG_ERROR, _mark, " not in records", LOG_END );

                return NULL;
            }

            inline void
            elapsed_from( const std::string _mark )
            {
                double elapsed;
                size_t iterat;
                try
                {
                     elapsed = close_stamp(_mark);
                     iterat = find_identifier( _mark.c_str() );
                }
                catch( Exception& e )
                {
                    switch( e.level() )
                    {
                        case Fail:{
                            LOG( LOG_WARNING, e.what(), LOG_END);
                            return;
                        }
                        default:{
                            throw e;
                        }

                      return;
                    }
                }

                LOG(   LOG_INFO
                   ,   TEXT_LOW_ON
                   ,   "Elapsed time from [", _mark, "]: "
                   ,   elapsed
                   ,   LOG_END
                   );
            }

            inline void
            show_consume( unsigned int pos = 0 )
            {

                FILE* file = fopen("/proc/self/status", "r");
                char line[128];

                static auto parseLine = [&](char* line)
                              {
                                  int i = strlen(line);
                                  const char* p = line;
                                  while (*p <'0' || *p > '9') p++;
                                  line[i-3] = '\0';
                                  i = atoi(p);
                                  return i;
                              };


                while (fgets(line, 128, file) != NULL)
                {
                    if (strncmp(line, "VmRSS:", 6) == 0)
                    {
                        LOG(  LOG_INFO
                           ,  "RAM usage: "
                           ,  parseLine(line)
                           ,  END_COLOR
                           );

                        break;
                    }
                }

                fclose(file);
            }

            inline void
            show_timestaps() const noexcept
            {
                LOG(   LOG_INFO
                   ,   "### Benchmarks history: \n"
                   );

                for( auto& i : Records )
                {
                    LOG(  CURSOR_MOVE_RIGHT(5)
                       ,  i.first
                       ,  " -> "
                       ,  i.second
                       ,  '\n'
                       );
                }
                LOG( LOG_END );
            }


    private:

        using identifier = std::pair<const char*, unsigned long >;
        using record = std::pair<const char*, double >;

        std::unordered_map<unsigned long, clock_t> Stamps;

        std::vector<identifier> labels;
        std::vector<record> Records;

        void
        put( const std::string& _mark )
        {
            unsigned long id = Utils::str2int( _mark.c_str() );

            if( find_identifier(_mark) == 0 )
            {
                labels.push_back(
                    std::make_pair( _mark.c_str(), id )
                );

                Stamps.insert(
                    {id, clock()}
                );
            }
            else

                LOG ( LOG_ERROR
                    , "Label Already Registered"
                    , LOG_END
                    );

            DEBUG_LOG( LOG_INFO, " Mark ", _mark, " with id: ", id, LOG_END );
        }

        inline size_t
        find_identifier( const std::string& _label ) const noexcept
        {
            unsigned long id = Utils::str2int( _label.c_str() );
            DEBUG_LOG( LOG_INFO, " Mark ", _label, " with id: ", id, LOG_END );

            if( labels.size() <= 1 ) return 0;

            for( size_t i = labels.size()-1 ; i!=0 ; i-- )
               if( labels.at(i).second == id )
                 return i;

            return 0;
        }

        inline clock_t const*
        find_stamp( const std::string _label ) const noexcept
        {
            unsigned long id = Utils::str2int( _label.c_str() );

            if( Stamps.size() >= 1 )
                return &Stamps.at(id);

            return nullptr;
        }

    }; // Watcher

} // namespace Benchmark

} // namespace Verbose
} // namespace ForBy

#endif // FORBY_BENCHMARK_INCLUDED
