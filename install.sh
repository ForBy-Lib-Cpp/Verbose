
#!/bin/bash


if [ $USER != 'root' ]
then
    echo '[!] User must be root for global installation'
    exit
fi

. ./ENVIROVEMENT

### Installing Sources

if [ $INCLUDE_SRC ]
then

CPP_DIR="/usr/src/ForBy/$NAME"

    if ! [ -d $CPP_DIR ]
    then
        mkdir $CPP_DIR
        cp ./src $CPP_DIR
    else
        rm -rf $CPP_DIR/*
    fi

    cp ./src/* $CPP_DIR -R

    if [ $? != 0 ]
    then
        echo "[!] Unspected error ocurred."
    else
        echo " >> $NAME $VERSION Installed correctly"
    fi
fi

### Installing Headers


if [ $INCLUDE_HPP ]
then

    HPP_DIR="/usr/include/ForBy/$NAME"

    if ! [ -d $HPP_DIR ]
    then
        mkdir $HPP_DIR
        cp ./src $HPP_DIR
    else
        rm -rf $HPP_DIR/*
    fi

    cp ./include/* $HPP_DIR -R

    if [ $? != 0 ]
    then
        echo "[!] Unspected error ocurred."
    else
        echo " >> $NAME $VERSION Installed correctly"
    fi
fi
