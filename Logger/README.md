
# Logger Tools
* Version 0.3.4 *



## Logger.hpp

  > Use [ -DENABLE_VERBOSE_LOG  ] for show Log messages on console.
  > Use [ -DFORBY_DEBUG ] to enable envirovement debug set ( supported by full ForBy namespace ).

  Logger is meant to be used as a Singletone for Logs.

  When ENABLE_VERBOSE_LOG flag is not set, by default it instances staticaly and
  pipes every LOG call to it so LOGs are pushed to runtime.log logfile.
  Else, it throws logs onto the console

  Since v0.3.3, Logger depends on ConsoleCodes. This means cursor control and more
  console stuff was added (refer to ConsoleCodes docs).

  RECORD_LOG() will always record arguments onto logfile.

### Example

  Showing logs:

  >  LOG( CONSOLE_CLEAR_FULL, "Foo is starting ", LOG_END);
  >  DEBUG_LOG( LOG_INFO, "This will only be shown when FORBY_DEBUG is enabled", LOG_END);


  Instantiating a Logfile:

  > #include <ForBy/Logger/Logger.hpp>

  > ForBy::Logger::FileLog another_logfile("another_logs.log");

  > another_logfile.log( LOG_INFO, "Pretty useful info", LOG_END);
  > another_logfile.log( LOG_WARNING, "Some warning", LOG_END);


## RuntimeInfo.hpp

  > Use [ -DDEFAULT_ADDRES  "new_address"] to change default server address.



## Dependencies

  > ForBy ConsoleCodes >= v0.1
  > ForBy Exception >= v0.2
