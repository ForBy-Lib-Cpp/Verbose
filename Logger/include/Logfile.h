//   ForBy ltd
///  Logfile v0.2

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FORBY_LOGGER_LOGFILE_INCLUDED
#define FORBY_LOGGER_LOGFILE_INCLUDED

#define _forby_logger_logfile_edition 0x0023

#define LOGGER_DEFAULT_LOG_FILENAME "runtime.log"

#include <ForBy/Exception.hpp>

#include <fstream>
#include <string>
#include <stdexcept>
#include <sstream>
#include <iostream>
#include <ctime>

namespace ForBy{
namespace Verbose{
namespace Logger
{

class Logfile
{

public:

    Logfile( std::string _file = LOGGER_DEFAULT_LOG_FILENAME)
    {
        FileLog.open( _file , std::ios::app);
        if(FileLog.bad())
            throw Exception( "Could not open logfile: " + FileLog.exceptions(), Error, true );
    }

    virtual ~Logfile()
    {
        FileLog.close();
    }

    template<typename _M, typename... _I>
    void log( _M&& _msg, _I&&... _info)
    {
        if(FileLog.good())
        {
            using args = int[];
            (void)args{0, (void( FileLog << std::forward<_I>(_info)),0)...};
        }
        else throw Exception("Bad I/O in FileLog", Error, true );
    }

protected:

    std::ofstream FileLog;

};

} // namespace Logger
} // namespace Verbose
} // namespace ForBy


#endif // FORBY_LOGGER_LOGFILE_INCLUDED
