//   ForBy ltd
///  Logger Tool v0.4

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FORBY_LOGGER_INCLUDED
#define FORBY_LOGGER_INCLUDED

#define _forby_logger_edition 0x0040

#include <ForBy/Utils/Time.hpp>
#include <ForBy/Logger/Logfile.h>
#include <ForBy/Logger/ConsoleCodes.h>

namespace ForBy{
namespace Verbose{
namespace Logger
{


template <typename... Args>
void _log_(Args&&... args)
{
    using expander = int[];
    (void)expander{
        0 ,
        ( void(std::cout << std::forward<Args>(args)),0)...
    };

}


//=================================
//      Message Manipulators

/*  IDENTIFIERS SET */


#define IS_SUCCESS     " [+] "
#define IS_WARNING     " [~] "
#define IS_ERROR       " [!] "
#define IS_INFO        " [i] "
#define IS_HEADER      " ==> "
#define IS_SHOW        "  >> "
#define IS_PROGRESS    "  -> "


/*################*/


/* Context iterators */

#define RECORD_LOG(...) ForBy::Verbose::Logger::_log_(__VA_ARGS__)


#ifdef ENABLE_VERBOSE_LOG

    #define LOG_SUCCESS     COLOR_BR_GREEN,   IS_SUCCESS
    #define LOG_WARNING     COLOR_BR_YELLOW,  IS_WARNING
    #define LOG_ERROR       COLOR_RED,        IS_ERROR
    #define LOG_INFO        COLOR_CYAN,       IS_INFO
    #define LOG_HEADER      COLOR_BR_MAGENTA, IS_HEADER
    #define LOG_SHOW        COLOR_BR_WHITE,   IS_SHOW
    #define LOG_PROGRESS    COLOR_BR_BLUE,    IS_PROGRESS
    #define LOG_END         END_COLOR,        TEXT_RESET,   '\n'

    #define LOG(...) ForBy::Verbose::Logger::_log_(__VA_ARGS__)


#else

    static Logfile _logfile;

    //=================

    #define LOG_SUCCESS     ForBy::Utils::get_date(), IS_SUCCESS
    #define LOG_WARNING     ForBy::Utils::get_date(), IS_WARNING
    #define LOG_ERROR       ForBy::Utils::get_date(), IS_ERROR
    #define LOG_INFO        ForBy::Utils::get_date(), IS_INFO
    #define LOG_HEADER      ForBy::Utils::get_date(), IS_HEADER
    #define LOG_SHOW        ForBy::Utils::get_date(), IS_SHOW
    #define LOG_PROGRESS    ForBy::Utils::get_date(), IS_PROGRESS
    #define LOG_END         '\n'

    #define LOG(...) ForBy::Verbose::Logger::_logfile.log( __VA_ARGS__ )


#endif // ENABLE_VERBOSE_LOG


#ifndef FORBY_DEBUG
    #define DEBUG_LOG(...) ""

#else
    #define DEBUG_LOG(...) ForBy::Verbose::Logger::_log_(__VA_ARGS__)

#endif

} // namespace Logger
} // namespace Verbose
} // namespace ForBy

#endif // FORBY_LOGGER_INCLUDED
