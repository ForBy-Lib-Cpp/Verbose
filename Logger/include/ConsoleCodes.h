//  ForBy LTD 2017
// Console Codes

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FORBY_LOGGER_CONSOLE_CODES_INCLUDED
#define FORBY_LOGGER_CONSOLE_CODES_INCLUDED

#define _forby_common_consolecodes_edition 0x0011

// ------------------

    /* Text Color set  */

    #define COLOR_BLACK        "\033[30m"
    #define COLOR_RED          "\033[31m"
    #define COLOR_GREEN        "\033[32m"
    #define COLOR_YELLOW       "\033[33m"
    #define COLOR_BLUE         "\033[34m"
    #define COLOR_MAGENTA      "\033[35m"
    #define COLOR_CYAN         "\033[36m"
    #define COLOR_WHITE        "\033[37m"

    #define COLOR_BR_BLACK     "\033[30;1m"
    #define COLOR_BR_RED       "\033[31;1m"
    #define COLOR_BR_GREEN     "\033[32;1m"
    #define COLOR_BR_YELLOW    "\033[33;1m"
    #define COLOR_BR_BLUE      "\033[34;1m"
    #define COLOR_BR_MAGENTA   "\033[35;1m"
    #define COLOR_BR_CYAN      "\033[36;1m"
    #define COLOR_BR_WHITE     "\033[37;1m"

    #define END_COLOR          "\033[39m"


// -------------------

    /* Background Color set */

    #define BG_COLOR_BLACK        "\033[40m"
    #define BG_COLOR_RED          "\033[41m"
    #define BG_COLOR_GREEN        "\033[42m"
    #define BG_COLOR_BROWN        "\033[43m"
    #define BG_COLOR_BLUE         "\033[44m"
    #define BG_COLOR_MAGENTA      "\033[45m"
    #define BG_COLOR_CYAN         "\033[46m"
    #define BG_COLOR_WHITE        "\033[47m"

    #define BG_COLOR_BR_BLACK     "\033[40;1m"
    #define BG_COLOR_BR_RED       "\033[41;1m"
    #define BG_COLOR_BR_GREEN     "\033[42;1m"
    #define BG_COLOR_BR_BROWN     "\033[43;1m"
    #define BG_COLOR_BR_BLUE      "\033[44;1m"
    #define BG_COLOR_BR_MAGENTA   "\033[45;1m"
    #define BG_COLOR_BR_CYAN      "\033[46;1m"
    #define BG_COLOR_BR_WHITE     "\033[47;1m"


//----------------

    /* Console iterators */

    #define CONSOLE_RESET         "\033[3J]"
    #define CONSOLE_CLEAR         "\033[2J\033[1;1H"


    #define LINE_CLEAR_FULL       "\033[2K"
    #define LINE_CLEAR_TO_CURSOR  "\033[1K"

    #define LINE_ERASE            LINE_CLEAR_FULL, "\r"

    #define CONSOLE_EMPTY_LINES( X ) \
         "\033[" #X "L"



//-----------------

    /* Console tools */

    #define CONSOLE_BELL          "\a"
    #define CONSOLE_BACKSPACE     "\b"
    #define CONSOLE_VERTICAL_TAB  "\v"
    #define CONSOLE_NEW_PAGE      "\f"

//----------------

    /* Text tools */

    #define TEXT_RESET          "\033[0m"
    #define TEXT_HIDDEN         "\033[8m"

    #define TEXT_BOLD_ON        "\033[1m"
    #define TEXT_BOLD_OFF       "\033[21m"

    #define TEXT_LOW_ON         "\033[2m"
    #define TEXT_LOW_OFF        "\033[22m"

    #define TEXT_BLINK_ON       "\033[5m"
    #define TEXT_BLINK_OFF      "\033[25m"

    #define TEXT_UNDERSCORE_ON  "\033[4m"
    #define TEXT_UNDERSCORE_OFF "\033[24m"


    #define TEXT_UNDERSCORED( _X_ )   \
        TEXT_UNDERSCORE_ON _X_ TEXT_UNDERSCORE_OFF

    #define TEXT_BLINKING( _X_ )      \
        TEXT_BLINK_ON _X_ TEXT_BLINK_OFF

    #define TEXT_TO_LOW( _X_ )        \
        TEXT_LOW_ON _X_ TEXT_LOW_OFF

    #define TEXT_TO_BOLD( _X_ )       \
        TEXT_BOLD_ON _X_ TEXT_BOLD_OFF


//----------------

  /* Miscelaneous Keys */

  #define KEYCODE_ARROW_UP \033[


//----------------

  /* Cursor Manipulator */

  #define CURSOR_CARRIAGE_RETURN  "\r"


  #define CURSOR_POS_SAVE         '\033[s'
  #define CURSOR_POS_RESTORE      '\033[u'

  #define CURSOR_CATCH_CURRENT    '\033[6n'

  #define CURSOR_JUMP( X, Y )   \
      "\033[" #X "," #Y "H"

  #define CURSOR_MOVE_UP( X )     \
      "\033[" #X "A"

  #define CURSOR_MOVE_DOWN( X )   \
      "\033[" #X "B"

  #define CURSOR_MOVE_RIGHT( X )  \
      "\033[" #X "C"

  #define CURSOR_MOVE_LEFT( X )   \
      "\033[" #X "D"

  #define CURSOR_TO_ROW( X )      \
      "\033[" #X "d"


#endif // FORBY_LOGGER_CONSOLE_CODES_INCLUDED
